package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"farmcycle.us/user/farmcycle/api"
	"farmcycle.us/user/farmcycle/api/customer"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/sessions"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func testPost(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello World"))
}

// unrestrictedAccess
func unrestrictedAccess(uri string) bool {
	const base = "/api/"
	const register = base + "register"
	const isAuth = base + "isAuth"
	const login = base + "login"
	const allProductsByTag = base + "allProductsByTag"
	const productsByKeyword = base + "productsByKeyword"
	const farmInfoByID = base + "farmInfoById"
	const test = base + "test"

	return uri == register || uri == isAuth || uri == allProductsByTag || uri == productsByKeyword || uri == farmInfoByID || uri == login || uri == test
}

// AuthMiddleware a middleware for insuring that
// the user is authenticated
func AuthMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		isAuthed := sessions.Authenticated(r)

		// If a user is logging or registering, they don't need to
		// authorisation to do so
		if unrestrictedAccess(r.RequestURI) == false && isAuthed == false {
			http.Error(w, "Forbidden", http.StatusForbidden)
		} else {
			w.Header().Set("Content-Type", "application/json")
			h.ServeHTTP(w, r)
		}
	})
}

// LogMiddleware logs when a request has been made
func LogMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log := logger.RouteLog(r)
		log.Info()
		h.ServeHTTP(w, r)
	})
}

func main() {
	port := os.Getenv("PORT")
	// Default port is 8000
	if port == "" {
		port = "8000"
	}
	fmt.Println("Serving on port", port)

	// Init Router
	router := mux.NewRouter().PathPrefix("/api").Subrouter()
	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD"})
	origins := handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED")})
	credentials := handlers.AllowCredentials()
	router.Use(AuthMiddleware, LogMiddleware)

	// Route Handlers/Endpoints

	// Customer Cart API
	router.HandleFunc("/cartByCustomerId", customer.GetCartByCustomerID).Methods("POST")
	router.HandleFunc("/addToCart", customer.AddToCart).Methods("POST")
	router.HandleFunc("/removeFromCart", customer.RemoveFromCart).Methods("POST")
	router.HandleFunc("/cartSize", customer.GetCartSize).Methods("GET")
	router.HandleFunc("/updateCartQuantity", customer.UpdateCartQuantity).Methods("POST")
	router.HandleFunc("/productDetailsInCart", customer.GetProductDetailsInCart).Methods("GET")

	// Customer Wishlist API
	router.HandleFunc("/addToWishlist", customer.AddToWishlist).Methods("POST")
	router.HandleFunc("/removeFromWishlist", customer.RemoveFromWishlist).Methods("POST")
	router.HandleFunc("/isFavorited", customer.IsFavorited).Methods("POST")
	router.HandleFunc("/wishlistByCustomerId", customer.GetWishlistByCustomerID).Methods("GET")

	// Customer Profile API
	router.HandleFunc("/addCustomer", customer.AddCustomer).Methods("POST")
	router.HandleFunc("/addShippingAddress", customer.AddShippingAddress).Methods("POST")
	router.HandleFunc("/shippingAddresses", customer.GetShippingAddresses).Methods("GET")
	router.HandleFunc("/removeShippingAddress", customer.RemoveShippingAddress).Methods("POST")
	router.HandleFunc("/updatePhoneNumber", customer.UpdatePhoneNumber).Methods("POST")
	router.HandleFunc("/phoneNumberById", customer.GetPhoneNumber).Methods("GET")

	// Payment API
	router.HandleFunc("/makeCheckoutSession", customer.MakeCheckoutSession).Methods("POST")
	router.HandleFunc("/confirmCheckout", customer.ConfirmCheckout).Methods("POST")

	// Farmers API
	router.HandleFunc("/addFarmers", api.AddFarmers).Methods("POST")
	router.HandleFunc("/allFarmers", api.AllFarmers).Methods("GET")
	router.HandleFunc("/farmerExist", api.FarmerExist).Methods("POST")
	router.HandleFunc("/farmInfoById", api.GetFarmerInfoByID).Methods("POST")

	// Products API
	router.HandleFunc("/addProduct", api.AddProduct).Methods("POST")
	router.HandleFunc("/addProducts", api.AddProducts).Methods("POST")
	router.HandleFunc("/allProductsByTag", api.AllProductsByTag).Methods("GET")
	router.HandleFunc("/productsByIds", api.GetProductsByIDs).Methods("POST")
	router.HandleFunc("/productsByKeyword", api.SearchProducts).Methods("POST")

	// Admin API
	router.HandleFunc("/register", api.Register).Methods("POST")
	router.HandleFunc("/login", api.Login).Methods("POST")
	router.HandleFunc("/isAuth", api.IsAuth).Methods("GET")
	router.HandleFunc("/logout", api.Logout).Methods("GET")

	// Test
	router.HandleFunc("/test", testPost).Methods("GET")

	log.Fatal(http.ListenAndServe(":"+port, handlers.CORS(headers, methods, origins, credentials)(router)))
}
