package helper

import (
	"fmt"
	"reflect"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// StrToObjectIDArr converts an array of ids represented as string in
// an array of ids represented as objectIds.
func StrToObjectIDArr(ids []string) ([]primitive.ObjectID, error) {
	oids := []primitive.ObjectID{}

	for _, id := range ids {
		oid, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			return nil, err
		}
		oids = append(oids, oid)
	}
	return oids, nil
}

// MapToObjIDArr converts a map[string]interface{} into an array of
// ObjectIDs, and error if it occurs. If there is no error, it returns nil
func MapToObjIDArr(m interface{}) ([]primitive.ObjectID, error) {
	mType := reflect.ValueOf(m)
	if mType.Kind() == reflect.Map && reflect.TypeOf(m).Key().Kind() == reflect.String {
		oids := []primitive.ObjectID{}
		for _, key := range mType.MapKeys() {
			oid, err := primitive.ObjectIDFromHex(key.String())
			if err != nil {
				return nil, err
			}
			oids = append(oids, oid)
		}
		return oids, nil
	}
	return nil, fmt.Errorf("parameter is not a map[string]interface{} type")
}
