module farmcycle.us/user/farmcycle

go 1.14

require (
	cloud.google.com/go/firestore v1.2.0 // indirect
	cloud.google.com/go/storage v1.10.0 // indirect
	firebase.google.com/go v3.13.0+incompatible
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/gomodule/redigo v1.8.2
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/sessions v1.2.0
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/sendgrid/rest v2.6.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.6.3+incompatible
	github.com/sirupsen/logrus v1.6.0
	github.com/stripe/stripe-go v70.15.0+incompatible
	github.com/stripe/stripe-go/v71 v71.48.0
	go.mongodb.org/mongo-driver v1.3.5
	google.golang.org/api v0.29.0 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	labix.org/v2/mgo v0.0.0-20140701140051-000000000287
)
