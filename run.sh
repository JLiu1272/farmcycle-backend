#!/bin/bash

# Run this when you first start the project
# source ./sendgrid.env

go install farmcycle.us/user/farmcycle
export PATH=$PATH:$(dirname $(go list -f '{{.Target}}' .))
farmcycle
