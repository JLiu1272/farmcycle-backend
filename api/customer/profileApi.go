package customer

import (
	"context"
	"encoding/json"
	"net/http"

	"farmcycle.us/user/farmcycle/api"
	"farmcycle.us/user/farmcycle/sessions"

	"farmcycle.us/user/farmcycle/database/customercollection"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
)

// AddCustomer - add customer to database
func AddCustomer(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	// Deserialise json
	var customer model.Customer
	decoded := json.NewDecoder(r.Body)
	decoded.Decode(&customer)

	// Ensure that post data has all the required fields of customer
	if err := api.ValidateInput(customer); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := customercollection.InsertCustomer(ctx, customer); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Customer added successfully",
	})
}

// AddShippingAddress adds shipping address to customer's list of address
func AddShippingAddress(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	var address model.Address
	if err := json.NewDecoder(r.Body).Decode(&address); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(address); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	customercollection.InsertShippingAddress(ctx, userID, address)

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Shipping address added successfully",
	})
}

// GetShippingAddresses obtains an of addresses by the customer id
func GetShippingAddresses(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	addresses, err := customercollection.QueryShippingAddresses(ctx, userID)
	if err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(addresses)
}

// RemoveShippingAddress removes the address from the list of addresses
// for the customer
func RemoveShippingAddress(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	var data struct {
		AddressLine1 string `json:"addressLine1"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := customercollection.RemoveAddress(ctx, userID, data.AddressLine1); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Shipping address successfully deleted",
	})
}

// UpdatePhoneNumber updates the user's phone number
func UpdatePhoneNumber(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	var data struct {
		PhoneNumber string `validate:"required" json:"phoneNumber"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(data); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := customercollection.SetNewPhoneNumber(ctx, userID, data.PhoneNumber); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Phone number successfully updated",
	})
}

// GetPhoneNumber api endpoint for obtaining customer's
// phone number. Customer ID is provided in the cookie.
func GetPhoneNumber(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	phoneNumber, err := customercollection.PhoneNumberByCustomerID(ctx, userID)

	if err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(phoneNumber)
}
