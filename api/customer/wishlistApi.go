package customer

import (
	"context"
	"encoding/json"
	"net/http"

	"farmcycle.us/user/farmcycle/api"
	"farmcycle.us/user/farmcycle/database/customercollection"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"farmcycle.us/user/farmcycle/sessions"
)

// AddToWishlist adds the given product id to
// the customer's wishlist array
func AddToWishlist(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	var wishlist model.Wishlist
	userID := sessions.GetUserID(r).(string)
	if err := json.NewDecoder(r.Body).Decode(&wishlist); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(wishlist); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := customercollection.InsertToWishlist(ctx, userID, wishlist.ProductIDs); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Product ids successfully added to wishlist",
	})
}

// RemoveFromWishlist removes the array of product
// ids from the wishlist of the customer
func RemoveFromWishlist(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	var wishlist model.Wishlist
	userID := sessions.GetUserID(r).(string)
	if err := json.NewDecoder(r.Body).Decode(&wishlist); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(wishlist); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := customercollection.DeleteFromWishlist(ctx, userID, wishlist.ProductIDs); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Product ids successfully removed from wishlist",
	})
}

// IsFavorited checks whether the given productId for this
// customer is favorited or not
func IsFavorited(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	var wishlist model.Wishlist
	userID := sessions.GetUserID(r).(string)
	if err := json.NewDecoder(r.Body).Decode(&wishlist); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(wishlist); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	isFavorited, err := customercollection.IsFavoritedByCustomerID(ctx, userID, wishlist.ProductIDs)
	if err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	result := map[string]bool{
		"isFavorited": isFavorited,
	}

	json.NewEncoder(w).Encode(result)
}

// GetWishlistByCustomerID obtains the wishlist array
// using the customer id stored in the cookie
func GetWishlistByCustomerID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	wishlist, err := customercollection.FindWishlistByCustomerID(ctx, userID)
	if err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	// Serialize wishlist. This becomes
	// the request's response
	json.NewEncoder(w).Encode(wishlist)
}
