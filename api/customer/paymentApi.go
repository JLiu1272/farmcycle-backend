package customer

import (
	"context"
	"encoding/json"
	"net/http"

	"farmcycle.us/user/farmcycle/api"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"farmcycle.us/user/farmcycle/payment"
)

// MakeCheckoutSession Generate a session id to be
// used for redirecting to stripe page
func MakeCheckoutSession(w http.ResponseWriter, r *http.Request) {

	ctx := context.Background()
	var data struct {
		Currency string          `validate:"required" json:"currency"`
		Items    []model.Product `validate:"required" json:"items"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(data); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	sessionID, err := payment.CreateCheckoutSession(ctx, data.Currency, data.Items)
	if err != nil {
		api.HandleBadReqErr(err.Msg, logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{
		"sessionID": sessionID,
	})
}

// ConfirmCheckout sends a receipt confirmation with all the
// user's information to order@farmcycle.us
func ConfirmCheckout(w http.ResponseWriter, r *http.Request) {
	var receiptContent model.Receipt
	if err := json.NewDecoder(r.Body).Decode(&receiptContent); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(receiptContent); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	err := payment.SendReceipt(receiptContent)

	if err != nil {
		api.HandleInternalServerError(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Receipt confirmation sent",
	})
}
