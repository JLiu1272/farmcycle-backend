package customer

import (
	"context"
	"encoding/json"
	"net/http"

	"farmcycle.us/user/farmcycle/api"
	"farmcycle.us/user/farmcycle/database/customercollection"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"farmcycle.us/user/farmcycle/sessions"
)

// AddToCart - Adds items to the cart.
// The request should be a json object containing productIds and a customerId
// Example request body:
// { productIds: [productId1, productId2, ...], customerId: "xxx" }
func AddToCart(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	userID := sessions.GetUserID(r)
	cart := model.Cart{}
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(cart); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := customercollection.InsertToCart(ctx, userID.(string), cart); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Items inserted to cart successfully",
	})
}

// RemoveFromCart removes the item from cart given
// the products to remove and the customer id
func RemoveFromCart(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)
	cart := model.Cart{}
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(cart); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := customercollection.DeleteFromCart(
		ctx,
		userID,
		cart.Items,
	); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	// Return a message indicating
	// the transaction was successful
	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Items removed from cart successfully",
	})
}

// GetCartByCustomerID gets the cart detail of customer
func GetCartByCustomerID(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Important to initialise a new context for each API
	// call to prevent race condition
	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	cart, err := customercollection.FindCartByCustomerID(ctx, userID)
	if err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(cart)
}

// UpdateCartQuantity given a customer id, and a product id,
// update the quantity of the product
func UpdateCartQuantity(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	cart := model.Cart{}
	if err := json.NewDecoder(r.Body).Decode(&cart); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := api.ValidateInput(cart); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	// Update the cart quantity given the customer id,
	// and product id
	if err := customercollection.ChangeCartQuantity(ctx, userID, cart.Items); err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Quantity of products in cart updated successfully",
	})
}

// GetCartSize gets the cart size of the customer id's
// cart
func GetCartSize(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	userID := sessions.GetUserID(r).(string)

	// Update the cart quantity given the customer id,
	// and product id
	size, err := customercollection.CartSize(ctx, userID)
	if err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(struct {
		Size int
	}{Size: size})
}

// GetProductDetailsInCart matches the products in user cart
// to the products details
func GetProductDetailsInCart(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	ctx := context.Background()
	customerID := sessions.GetUserID(r).(string)

	productDetails, err := customercollection.QueryProductDetailsInCart(ctx, customerID)
	if err != nil {
		api.HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(productDetails)
}
