package api

import (
	"context"
	"encoding/json"
	"net/http"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
)

// AddFarmers - add farmer to Farmers Collection
func AddFarmers(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	var farmers []model.Farmer
	if err := json.NewDecoder(r.Body).Decode(&farmers); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	database.InsertFarmers(ctx, farmers)

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Farmers added successfully",
	})
}

// AllFarmers retrieves all of the farmers in the database
func AllFarmers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Important to initialise a new context for each API
	// call to prevent race condition
	ctx := context.Background()
	farmers := database.FindAllFarmers(ctx)
	json.NewEncoder(w).Encode(farmers)

	// Logging
	log := logger.RouteLog(r)
	log.Info("allFarmers")
}

// GetFarmerInfoByID gets the farmer's info by its id
func GetFarmerInfoByID(w http.ResponseWriter, r *http.Request) {
	data := struct {
		FarmerID string `validate:"required" json:"farmerId"`
	}{}

	// Decode the body into a map datastructure
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := ValidateInput(data); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	ctx := context.Background()
	farmerInfos, err := database.FindFarmerByID(ctx, data.FarmerID)
	if err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if len(farmerInfos) == 0 {
		json.NewEncoder(w).Encode(model.HTTPMsg{
			Action:  logger.FuncName(),
			Message: "Farmer ID does not exist",
		})
	} else {
		json.NewEncoder(w).Encode(farmerInfos[0])
	}
}

// FarmerExist - Given farmer's id, find whether this farmer
// exist in the farmer's collection or not
func FarmerExist(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Anonymous struct type: designed for one time use
	idStruct := struct {
		FarmerID *string `validate:"required" json:"farmerId"`
	}{}

	// Decode the body into a map datastructure
	if err := json.NewDecoder(r.Body).Decode(&idStruct); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := ValidateInput(idStruct); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	// got the input we expected: no more, no less
	// DEBUGGING: Print farmer Id out
	// fmt.Printf("%v", *idStruct.FarmerID)

	// Important to initialise a new context for each API
	// call to prevent race condition
	ctx := context.Background()
	hasFarmer, err := database.FarmerIDExist(ctx, *idStruct.FarmerID)
	if err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	result := map[string]bool{
		"hasFarmer": hasFarmer,
	}
	json.NewEncoder(w).Encode(result)
}
