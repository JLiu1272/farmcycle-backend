package api

import (
	"net/http"

	"farmcycle.us/user/farmcycle/logger"
	"gopkg.in/go-playground/validator.v9"
)

// HandleBadReqErr handles requests that had issue being processed. This
// will send a badrequest error response to the client
// @param
// errMsg - the actual error object
// logMsg - the message indicating line number, function name, and any other useful
// message for debugging the error
func HandleBadReqErr(errMsg string, logMsg string, w http.ResponseWriter, r *http.Request) {
	log := logger.RouteLog(r)
	http.Error(w, errMsg, http.StatusBadRequest)
	log.Errorf(logMsg+" %v", errMsg)
}

// HandleInternalServerError reports error that occur
// due internal system issue
func HandleInternalServerError(errMsg string, logMsg string, w http.ResponseWriter, r *http.Request) {
	log := logger.RouteLog(r)
	http.Error(w, errMsg, http.StatusInternalServerError)
	log.Errorf(logMsg+" %v", errMsg)
}

// ValidateInput validates whether the input provided
// in the request has all the required fields defined
// by input
func ValidateInput(input interface{}) error {
	validate := validator.New()
	err := validate.Struct(input)
	return err
}
