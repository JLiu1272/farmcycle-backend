package api

import (
	"context"
	"encoding/json"
	"net/http"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/database/productscollection"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
)

// AddProduct - add product to database
func AddProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	ctx := context.Background()
	var product model.Product
	_ = json.NewDecoder(r.Body).Decode(&product)
	database.InsertProduct(ctx, product)

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Product added successfully",
	})
}

// AddProducts - add product to database
// Need to ensure that the farmerId is the same for
// every product
func AddProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var products model.Products
	decoded := json.NewDecoder(r.Body)
	decoded.Decode(&products)

	ctx := context.Background()
	hasFarmer, err := database.FarmerIDExist(ctx, products.FarmerID.Hex())
	if hasFarmer == false || err != nil {
		errMsg := "Farmer Id does not exist"
		HandleBadReqErr(errMsg, logger.Log(), w, r)
		return
	}

	err = database.InsertProducts(ctx, products.Products, products.FarmerID)
	if err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "Products added successfully",
	})
}

// AllProductsByTag retrieves all of the products in the database
func AllProductsByTag(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Important to initialise a new context for each API
	// call to prevent race condition
	ctx := context.Background()
	products, err := productscollection.FindAllProductsByTag(ctx)
	if err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(products)
}

// GetProductsByIDs retrieves all of the products in the database
func GetProductsByIDs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var data struct {
		ProductIDs []string `validate:"required" json:"productIds"`
	}

	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := ValidateInput(data); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	ctx := context.Background()
	products, err := productscollection.FindProductByID(ctx, data.ProductIDs)
	if err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(products)
}

// SearchProducts searches product that has matching or contains parts of itemName
// with the given keyword
func SearchProducts(w http.ResponseWriter, r *http.Request) {
	var filter model.ProductFilter
	if err := json.NewDecoder(r.Body).Decode(&filter); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if err := ValidateInput(filter); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	ctx := context.Background()
	products, err := productscollection.FindProductByKeyword(ctx, filter.Keyword)
	if err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(products)
}
