package api

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/database/customercollection"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"farmcycle.us/user/farmcycle/sessions"

	firebase "firebase.google.com/go"
	firebaseAuth "firebase.google.com/go/auth"
)

// initalises Firebase setup configurations
func initFirebaseAuth() *firebaseAuth.Client {
	ctx := context.Background()
	app, err := firebase.NewApp(ctx, nil)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}

	client, err := app.Auth(ctx)
	if err != nil {
		log.Fatalf("error initializing firebase auth: %v\n", err)
	}
	return client

}

// IsAuth Checks
func IsAuth(w http.ResponseWriter, r *http.Request) {
	isAuthed := sessions.Authenticated(r)

	json.NewEncoder(w).Encode(map[string]bool{
		"isAuth": isAuthed,
	})
}

// Register - When registering, it first
// adds this customer to the database, then it
// logs them in
func Register(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()

	var data model.IDToken
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	client := initFirebaseAuth()

	// Verify that token is valid
	token, err := client.VerifyIDToken(ctx, data.IDToken)
	if err != nil {
		errMsg := fmt.Sprintf("Error verifying ID token: %v\n", err)
		HandleBadReqErr(errMsg, logger.Log(), w, r)
		return
	}

	userRecord, err := client.GetUser(ctx, token.UID)
	if err != nil {
		errMsg := fmt.Sprintf("Error obtaining user's information: %v\n", err)
		HandleBadReqErr(errMsg, logger.Log(), w, r)
		return
	}

	customerExist, err := customercollection.CustomerExist(ctx, userRecord.UID)
	if err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	if customerExist == false {
		// Create a new customer, and add it to
		// customer database
		newCustomer := model.NewCustomer(
			userRecord.UID,
			userRecord.PhoneNumber,
			userRecord.DisplayName,
			userRecord.Email,
		)

		if err := customercollection.InsertCustomer(ctx, newCustomer); err != nil {
			HandleBadReqErr(err.Error(), logger.Log(), w, r)
			return
		}
	}

	database.CustomerLog.Infof("New Customer Registered UID: %v", userRecord.UID)
	sessions.ManageUserSession(userRecord.UID, true, w, r)

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "User successfully registered",
	})
}

// Login - verifies whether this user has an account
func Login(w http.ResponseWriter, r *http.Request) {
	var data model.IDToken
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		HandleBadReqErr(err.Error(), logger.Log(), w, r)
		return
	}

	ctx := context.Background()
	client := initFirebaseAuth()

	token, err := client.VerifyIDToken(ctx, data.IDToken)
	if err != nil {
		errMsg := fmt.Sprintf("Error verifying ID token: %v\n", err)
		HandleBadReqErr(errMsg, logger.Log(), w, r)
		return
	}

	log := logger.RouteLog(r)
	log.Infof("User successfully signed in: %v", token.UID)

	if err := sessions.ManageUserSession(token.UID, true, w, r); err != nil {
		errMsg := fmt.Sprintf("Signing in failed while creating session: %v", err)
		HandleInternalServerError(errMsg, logger.Log(), w, r)
		return
	}

	json.NewEncoder(w).Encode(model.HTTPMsg{
		Action:  logger.FuncName(),
		Message: "User successfully signed in",
	})
}

// Logout - revokes the session for the particular user
func Logout(w http.ResponseWriter, r *http.Request) {
	log := logger.RouteLog(r)
	userSession, err := sessions.GetUserSession(r)
	if err != nil {
		errMsg := fmt.Sprintf("Ran into issue while retrieving user session: %v", err)
		HandleInternalServerError(errMsg, logger.Log(), w, r)
		return
	}

	log.Infof("UserId: %v signed out", userSession.Values["userid"])

	if err := sessions.ManageUserSession("", false, w, r); err != nil {
		errMsg := fmt.Sprintf("Logging out failed: %v", err)
		HandleInternalServerError(errMsg, logger.Log(), w, r)
		return
	}
}
