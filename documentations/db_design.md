### uml: class diagram
```plantuml
@startuml
package "Farmcycle Database" #DDDDDD {
    class Customer {
        + uid
        + phone
        + name
        + email 
        + shoppingCart: Cart 
        + shippingAddresses: Map<name: address> 
        + wishlist: Arraylist<Product>
        + favoriteFarmers: Arraylist<Farmers>
        + purchaseHistories: Arraylist<PurchaseHistory>
    }

    class Product {
        + uid: Int
        + farmerId: Farmers:uid
        + farmerName: String
        + itemName: String
        + thumbnail: String
        + status: String
        + price: Float32 
    }

    class Farmer {
        + uid: Int
        + name: String
        + thumbnail
        + description
        + registryDate 
        + products: Arraylist<Product>
        + rating: Int
    }

    class CartDetails {
        + productUid
        + sellerName
        + itemName
        + price
        + quantity
    }

    class Cart {
        + items: Map<Product_id: quantity> 
        + size: Int
    }

    class PurchaseHistory {
        + time
        + productId: Product:uid
        + shippingAddress
        + quantity
    }

    Customer *-* Cart
    Cart ---{ CartDetails
    Customer ---{ PurchaseHistory
    Customer ---{ Product
    Customer ---{ Farmer
    Farmer ---{ Product
}
@enduml
```