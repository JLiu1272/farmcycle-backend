package payment

import (
	"context"
	"math"
	"os"

	"farmcycle.us/user/farmcycle/constants"
	"farmcycle.us/user/farmcycle/model"

	"github.com/stripe/stripe-go/v71"
	"github.com/stripe/stripe-go/v71/checkout/session"
)

// populateLineItems takes an array of item in the cart and
// converts it into CHeckoutSessionLineItemParams object.
func populateLineItems(currency string, items []model.Product) []*stripe.CheckoutSessionLineItemParams {
	var lineItems []*stripe.CheckoutSessionLineItemParams
	for _, item := range items {
		roundPrice := math.Round(item.Price * 100)

		// Used for testing stripe error
		// roundPrice := item.Price
		lineItem := &stripe.CheckoutSessionLineItemParams{
			PriceData: &stripe.CheckoutSessionLineItemPriceDataParams{
				Currency: stripe.String(currency),
				ProductData: &stripe.CheckoutSessionLineItemPriceDataProductDataParams{
					Name: stripe.String(item.ItemName),
				},
				UnitAmountDecimal: stripe.Float64(roundPrice),
			},
			Quantity: stripe.Int64(int64(item.Quantity)),
		}
		lineItems = append(lineItems, lineItem)
	}
	return lineItems
}

// CreateCheckoutSession generates a session for the given checkout items
func CreateCheckoutSession(ctx context.Context, currency string, items []model.Product) (string, *stripe.Error) {
	stripe.Key = os.Getenv("STRIPE_KEY")

	params := &stripe.CheckoutSessionParams{
		PaymentMethodTypes: stripe.StringSlice([]string{
			"card",
		}),
		Mode:       stripe.String("payment"),
		LineItems:  populateLineItems(currency, items),
		SuccessURL: stripe.String(constants.FarmCycleMainDomain + "/payment_success"),
		CancelURL:  stripe.String(constants.FarmCycleMainDomain + "/cart"),
	}

	session, err := session.New(params)

	// If an error occurs, we need to convert into a stripe Error struct,
	// and return it so that we can obtain the message from the error
	// to be sent back to the API
	if err != nil {
		if stripeErr, ok := err.(*stripe.Error); ok {
			return "", stripeErr
		}
	}

	return session.ID, nil
}
