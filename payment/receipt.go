package payment

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"text/template"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// FillReceiptTplPlaceholder fills the placeholder inside the receipt template with receipt content
// data
func FillReceiptTplPlaceholder(receiptContent model.Receipt) (string, error) {

	parsedTpl, err := template.ParseFiles("payment/receiptTemplate.html")
	if err != nil {
		return "", err
	}

	var filledTpl bytes.Buffer
	if err := parsedTpl.Execute(&filledTpl, receiptContent); err != nil {
		return "", err
	}
	return filledTpl.String(), nil
}

// SendReceipt sends a confirmation receipt to order@farmcycle.us
// indicating someone has placed an order and we need to process it
func SendReceipt(receiptContent model.Receipt) error {

	from := mail.NewEmail("Jennifer Liu", "jenniferliu@farmcycle.us")
	subject := fmt.Sprintf("%v Order Receipt", receiptContent.Name)
	to := mail.NewEmail("Order", "order@farmcycle.us")
	plainTextContent := "Thank you for ordering from Farm Cycle"

	htmlContent, err := FillReceiptTplPlaceholder(receiptContent)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	message := mail.NewSingleEmail(from, subject, to, plainTextContent, htmlContent)
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))

	response, err := client.Send(message)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	// Handle server internal error from sendgrid API
	if response.StatusCode >= 200 && response.StatusCode < 300 {
		sendgridServerErr := errors.New(response.Body)
		database.HandleCustomerError(sendgridServerErr, logger.Log())
		return err
	}

	// FOR DEBUGGING PURPOSE ONLY
	// fmt.Println(response.StatusCode)
	// fmt.Println(response.Body)
	// fmt.Println(response.Headers)
	return nil
}
