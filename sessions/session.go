package sessions

import (
	"fmt"
	"net/http"
	"os"

	"farmcycle.us/user/farmcycle/logger"
	"github.com/gorilla/sessions"
)

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key = []byte("super-secret-key")
	// Store - global store for accessing cookies
	Store = sessions.NewCookieStore(key)
	// SessionCookieName is key name for SessionCookieName
	SessionCookieName = "gpAclaci"
)

// Authenticated - ensures that the caller of this API has access
// rights
func Authenticated(r *http.Request) bool {
	session, _ := Store.Get(r, SessionCookieName)

	// Check if user is authenticated
	if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {
		return false
	}

	// log := logger.RouteLog(r)
	// log.Infof("UserId: %v checked for authentication", session.Values["userid"])
	return true
}

// GetUserID retrieves the userid that is stored in the cookie
func GetUserID(r *http.Request) interface{} {
	session, _ := Store.Get(r, SessionCookieName)
	isAuthed := Authenticated(r)

	// Check if user is authenticated
	if isAuthed == false {
		return nil
	}

	logger.RouteLog(r)
	return session.Values["userid"]
}

// ManageUserSession creates/edits/deletes the state
// of user's information inside the cookie
func ManageUserSession(userID string, authenticated bool, w http.ResponseWriter, r *http.Request) error {
	session, err := Store.Get(r, SessionCookieName)
	mode := os.Getenv("MODE")

	if mode == "prod" {
		session.Options = &sessions.Options{
			MaxAge:   86400 * 7,
			Secure:   true,
			SameSite: http.SameSiteNoneMode,
		}
	} else if mode == "dev" {
		session.Options = &sessions.Options{
			MaxAge: 86400 * 7,
		}
	} else {
		err := fmt.Errorf("%v is not a valid mode. It can only be dev or prod", mode)
		fmt.Printf(err.Error())
	}

	if err != nil {
		return err
	}
	session.Values["userid"] = userID
	session.Values["authenticated"] = authenticated

	err = session.Save(r, w)

	if err != nil {
		return err
	}
	return nil
}

// GetUserSession gives the user the session
// object for retrieve values inside the session
func GetUserSession(r *http.Request) (*sessions.Session, error) {
	session, err := Store.Get(r, SessionCookieName)
	if err != nil {
		return nil, err
	}
	return session, nil
}
