package customercollection

import (
	"context"
	"fmt"
	"time"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/database/productscollection"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// InsertToCart - Appends an array of productIds to the cart.
// The size of the cart will also update accordingly
// Assumptions:
// 	- Cart quantity is always greater than 0
//  - item's product id definitely exist
func InsertToCart(ctx context.Context, customerID string, newCart model.Cart) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	oldCart, err := FindCartByCustomerID(ctx, customerID)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	combinedItems, size := mergeItems(newCart.Items, oldCart.Items)

	filter := bson.M{"_id": customerID}
	updateResult, err := database.CustomersCollection.UpdateOne(ctx,
		filter,
		bson.M{
			"$set": bson.M{
				"shoppingCart.items": combinedItems,
				"shoppingCart.size":  size,
			},
		},
	)

	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	database.CustomerLog.Infof("%v: Matched %v documents and updated %v documents.\n", logger.FuncName(), updateResult.MatchedCount, updateResult.ModifiedCount)
	return nil
}

// FindCartByCustomerID - Obtain a mapping of shopping cart
// given customer id.
func FindCartByCustomerID(ctx context.Context, customerID string) (model.Cart, error) {
	cart, err := queryShoppingCartByCustomerID(ctx, customerID)

	// Catch errors
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return cart.ShoppingCart, err
	}

	return cart.ShoppingCart, nil
}

// productsInUserCart determines whether the user has
// all the products given in the cart. Returns true if user contains
// the array of products, else false. Pass in an array of
// product ids, and the customer id
func productsInUserCart(ctx context.Context, customerID string, productIDs []string) bool {
	query := bson.M{}
	for _, id := range productIDs {
		query["shoppingCart.items."+id] = bson.M{
			"$exists": true,
		}
	}
	query["_id"] = customerID

	cart, err := queryShoppingCartByCustomerID(ctx, customerID)

	// Catch errors
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return false
	}

	productsExist := isSubsetOfCart(productIDs, cart.ShoppingCart.Items)
	return productsExist
}

// DeleteFromCart remove items from cart of the customer id  database
// and it also updates the size of the cart accordingly.
// Assumption is that the deletedProducts mapping has the correct
// quantity associated with it.
// Parameters
// 	- customerID (primitive.ObjectID)
//  - deletedProducts (map[string]int)
func DeleteFromCart(ctx context.Context, customerID string, deletedProducts map[string]int) error {
	// Get the total quantity size
	deleteSize := countQuantity(deletedProducts)

	// Store the updates object here
	var updatedDocument bson.M

	// Check to see if all the products exist in the cart for this given
	// user
	productIDs := []string{}
	for id := range deletedProducts {
		productIDs = append(productIDs, id)
	}

	// Check to see if user have the products that it wants to
	// delete
	productsInCart := productsInUserCart(ctx, customerID, productIDs)
	if productsInCart == false {
		errorMsg := "Products does not exist in user cart."
		return fmt.Errorf(errorMsg)
	}

	// Find a document that matches the customer id provided
	// and if found update
	if err := database.CustomersCollection.FindOneAndUpdate(ctx,
		bson.M{"_id": customerID},
		bson.M{
			// Convert the value of this mapping to an empty string
			"$unset": addPrefix(deletedProducts, "shoppingCart.items.", false),
			"$inc": bson.M{
				"shoppingCart.size": -deleteSize,
			},
		},
	).Decode(&updatedDocument); err != nil {
		return err
	}

	return nil
}

// ChangeCartQuantity updates the product quantity for each of the
// product id given inside the customer's cart.
// Assumption: ProductId specified exist in the cart
func ChangeCartQuantity(ctx context.Context, customerID string, productIDs map[string]int) error {
	// Find that the product ids exist
	// in the cart
	opts := options.FindOneAndUpdate().SetUpsert(false)
	var updatedDocument bson.M

	// Check to see if all the products exist in the cart for this given
	// user
	productIDArr := []string{}
	for id := range productIDs {
		productIDArr = append(productIDArr, id)
	}

	// Check to see if user have the products that it wants to
	// update
	productsInCart := productsInUserCart(ctx, customerID, productIDArr)
	if productsInCart == false {
		errorMsg := "One or more products do not exist in user cart."
		return fmt.Errorf(errorMsg)
	}

	// Remove product id from the cart.
	// Recompute the cart size and update it
	if err := database.CustomersCollection.FindOneAndUpdate(ctx,
		bson.M{"_id": customerID},
		bson.M{"$set": addPrefix(productIDs, "shoppingCart.items.", true)},
		opts).Decode(&updatedDocument); err != nil {
		return err
	}

	if err := recomputeCartSize(ctx, customerID); err != nil {
		return err
	}

	return nil
}

// CartSize returns the customer's cart size
func CartSize(ctx context.Context, customerID string) (int, error) {
	cart, err := queryShoppingCartByCustomerID(ctx, customerID)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return 0, err
	}

	return cart.ShoppingCart.Size, nil
}

// QueryProductDetailsInCart obtains the product details of each product
// in the cart
func QueryProductDetailsInCart(ctx context.Context, customerID string) (map[string]model.Product, error) {
	cart, err := queryShoppingCartByCustomerID(ctx, customerID)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return nil, err
	}

	productsDetails, err := productscollection.FindAllProductsByIds(ctx, cart.ShoppingCart.Items)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return nil, err
	}
	return productsDetails, nil
}

// recomputeCartSize the size cached may not match the total quantity
// after updating the quantity. Need to recompute the total
// cart size
func recomputeCartSize(ctx context.Context, customerID string) error {
	query := bson.M{"_id": customerID}
	cart, err := queryShoppingCartByCustomerID(ctx, customerID)

	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	newQuantity := countQuantity(cart.ShoppingCart.Items)

	var updatedDoc bson.D
	err = database.CustomersCollection.FindOneAndUpdate(
		ctx,
		query,
		bson.M{"$set": bson.M{"shoppingCart.size": newQuantity}},
	).Decode(&updatedDoc)

	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	return nil
}

// queryShoppingCartByCustomerID finds the shoppingCart details for a particular customer
func queryShoppingCartByCustomerID(ctx context.Context, customerID string) (model.CustomerCart, error) {
	var customerCart model.CustomerCart
	err := database.CustomersCollection.FindOne(ctx,
		bson.M{"_id": customerID},
		options.FindOne().SetProjection(bson.M{"shoppingCart": 1}),
	).Decode(&customerCart)

	return customerCart, err
}

// merge 1 or more cart together
func mergeItems(items ...map[string]int) (map[string]int, int) {
	combinedItems := make(map[string]int)
	size := 0

	for _, item := range items {
		for id, quantity := range item {
			if _, ok := combinedItems[id]; !ok {
				// This id does not exist in the combinedItems yet
				// so we add a new entry. This prevents
				// having duplicate key values
				combinedItems[id] = quantity
				size += quantity
			}
		}
	}
	return combinedItems, size
}

// Given the map of items, count the quantity
// for each item
func countQuantity(items ...map[string]int) int {
	size := 0
	for _, item := range items {
		for _, quantity := range item {
			size += quantity
		}
	}
	return size
}

// isSubsetOfCart checks to see if the product ids given my user
// is contained inside the cart items
func isSubsetOfCart(productIDs []string, cartItems map[string]int) bool {
	for _, id := range productIDs {
		// Check to see if the productID is in the
		// cart. If the id is not in the shopping
		// cart, shopping cart is not a subset of productIds
		if _, ok := cartItems[id]; !ok {
			return false
		}
	}
	return true
}

// addPrefix prepends the string specified in the argument
// to every key in the map. It returns a new map where
// each key is prepended with the prefix string,
// keepVal parameters indicates whether the value of the original
// key,value pair should be kept. If keepVal is true, the original
// value will be preserved, otherwise the value of the new mapping
// will be an empty string
func addPrefix(mapping map[string]int, prefix string, keepVal bool) map[string]interface{} {
	prefixedMap := make(map[string]interface{})
	for key := range mapping {
		if keepVal {
			prefixedMap[prefix+key] = mapping[key]
		} else {
			prefixedMap[prefix+key] = ""
		}
	}
	return prefixedMap
}
