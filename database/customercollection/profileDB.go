package customercollection

import (
	"context"
	"time"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// InsertCustomer - Insert a new customer into the collection
// Assumption:
// 	- The client side checks to make sure that
// 	  the size and the sum of items quantity are consistent
// Notes:
//  - If a user does not input a size, it initialises a size of 0
func InsertCustomer(ctx context.Context, customer model.Customer) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	// Let route log show error message. Otherwise we will
	// have duplicated error messages
	res, err := database.CustomersCollection.InsertOne(ctx, customer)
	if err != nil {
		return err
	}
	database.CustomerLog.Infof("NewCustomerID: %v", res.InsertedID)
	return nil
}

// CustomerExist determines whether the customer exist in the system
// or not
func CustomerExist(ctx context.Context, customerID string) (bool, error) {
	opts := options.FindOne()
	filter := bson.D{{"_id", customerID}}

	var customer model.Customer
	err := database.CustomersCollection.FindOne(ctx, filter, opts).Decode(&customer)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		database.HandleCustomerError(err, logger.Log())
		return false, err
	}
	return true, nil
}

// QueryShippingAddresses get customer's list of shipping addresses
func QueryShippingAddresses(ctx context.Context, customerID string) (map[string]model.Address, error) {
	opts := options.FindOne().SetProjection(bson.M{"shippingAddresses": 1})
	filter := bson.M{"_id": customerID}

	var customerAddresses model.CustomerAddresses

	err := database.CustomersCollection.FindOne(ctx, filter, opts).Decode(&customerAddresses)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return nil, err
	}
	return customerAddresses.Addresses, nil
}

// InsertShippingAddress adds a new shipping address to the shipping address array
// of the customer
func InsertShippingAddress(ctx context.Context, customerID string, address model.Address) {
	opts := options.FindOneAndUpdate()
	filter := bson.M{"_id": customerID}

	update := bson.M{
		"$set": bson.M{
			"shippingAddresses." + address.AddressLine1: address,
		},
	}

	var updatedDoc bson.M
	err := database.CustomersCollection.FindOneAndUpdate(ctx, filter, update, opts).Decode(&updatedDoc)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return
	}
}

// RemoveAddress removes the address from customer at removeIdx
func RemoveAddress(ctx context.Context, customerID string, addressLine1 string) error {
	filter := bson.M{"_id": customerID}
	update := bson.M{
		"$unset": bson.M{
			"shippingAddresses." + addressLine1: "",
		}}

	var updatedDoc bson.M
	err := database.CustomersCollection.FindOneAndUpdate(ctx, filter, update).Decode(&updatedDoc)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}
	database.CustomerLog.Infof("Shipping address removed for %v", customerID)
	return nil
}

// SetNewPhoneNumber sets a new phone number for customer
func SetNewPhoneNumber(ctx context.Context, customerID string, phoneNumber string) error {
	filter := bson.M{"_id": customerID}
	update := bson.M{
		"$set": bson.M{
			"phone": phoneNumber,
		},
	}

	var updatedDoc bson.M
	err := database.CustomersCollection.FindOneAndUpdate(ctx, filter, update).Decode(&updatedDoc)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}
	database.CustomerLog.Infof("Phone Number updated for %v", customerID)

	return nil
}

// PhoneNumberByCustomerID obtain the customer's phone number based on the customer's ID
func PhoneNumberByCustomerID(ctx context.Context, customerID string) (map[string]string, error) {
	filter := bson.M{"_id": customerID}
	opts := options.FindOne().SetProjection(bson.M{"phone": 1})

	var document map[string]string
	err := database.CustomersCollection.FindOne(ctx, filter, opts).Decode(&document)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return nil, err
	}

	// Prevent showing customer Id as a plain text to customer
	phoneNumber := map[string]string{
		"phone": document["phone"],
	}

	database.CustomerLog.Infof("Retrieved phone number successfully %v", customerID)
	return phoneNumber, nil
}
