package customercollection

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"go.mongodb.org/mongo-driver/bson"
)

// InsertToWishlist inserts a new favorite item
// to the wishlist array list
func InsertToWishlist(ctx context.Context, customerID string, productIDs []primitive.ObjectID) error {
	var updatedDoc bson.M
	err := database.CustomersCollection.FindOneAndUpdate(
		ctx,
		bson.M{"_id": customerID},
		bson.M{
			"$addToSet": bson.M{
				"wishlist": bson.M{
					"$each": productIDs,
				},
			},
		},
	).Decode(&updatedDoc)

	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	return nil
}

// DeleteFromWishlist removes the array of product Ids from
// the wishlist array
func DeleteFromWishlist(ctx context.Context, customerID string, productIDs []primitive.ObjectID) error {
	var updatedDoc bson.M
	err := database.CustomersCollection.FindOneAndUpdate(
		ctx,
		bson.M{"_id": customerID},
		bson.M{
			"$pullAll": bson.M{
				"wishlist": productIDs,
			},
		},
	).Decode(&updatedDoc)

	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return err
	}

	return nil
}

// IsFavoritedByCustomerID checks to see whether this product is
// in customer Id's wishlist
func IsFavoritedByCustomerID(ctx context.Context, customerID string, productIDs []primitive.ObjectID) (bool, error) {
	var result bson.M
	err := database.CustomersCollection.FindOne(ctx,
		bson.M{
			"_id": customerID,
			"wishlist": bson.M{
				"$in": productIDs,
			},
		},
	).Decode(&result)

	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		// No Document found where the customer has
		// these products in their wishlist
		if err == mongo.ErrNoDocuments {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

// FindWishlistByCustomerID finds the wishlist of the given
// customerID
func FindWishlistByCustomerID(ctx context.Context, customerID string) ([]model.Product, error) {

	// Find the document in the customer collection
	// that has matching customerID
	findCustomer := bson.D{
		{"$match", bson.M{"_id": customerID}},
	}

	// Do a left outer join. Extract
	// the product details from the array of productIds.
	// Here is an example of the input.
	// input -> wishlist: [ObjectId('edkinsdsd'), ObjectId('edkdsdsdsdsd')]
	// output -> [
	//	{product detail 1 with id:'edkinsdsd' ...},
	//	{product detail 2 with id:'edkdsdsdsdsd'...}
	// ]
	joinWithProduct := bson.D{{
		"$lookup", bson.M{
			"from":         "Products",
			"localField":   "wishlist",
			"foreignField": "_id",
			"as":           "extractedWishlist",
		},
	}}

	opts := options.Aggregate().SetMaxTime(2 * time.Second)
	cursor, err := database.CustomersCollection.Aggregate(ctx, mongo.Pipeline{findCustomer, joinWithProduct}, opts)
	if err := database.HandleCustomerError(err, logger.Log()); err != nil {
		return nil, err
	}

	// Traverse through the results, and
	// extract the extractedWishlist from the join results
	for cursor.Next(ctx) {
		// create a value into which the single document can be decoded
		var data struct {
			ExtractedWishlist []model.Product `json:"extractedWishlist" bson:"extractedWishlist"`
		}
		err := cursor.Decode(&data)
		if err := database.HandleCustomerError(err, logger.Log()); err != nil {
			return nil, err
		}
		return data.ExtractedWishlist, nil
	}

	// If there is no results, return an empty array
	return make([]model.Product, 0), nil
}
