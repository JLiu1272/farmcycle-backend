package database

import "farmcycle.us/user/farmcycle/logger"

var (
	// CustomersCollection a connection instance for customers
	CustomersCollection = Connect().Collection("Customers")

	// CustomerLog a logger instance for customer
	CustomerLog = logger.DbLog("ecommerce", "Customers")

	// FarmersCollection -
	FarmersCollection = Connect().Collection("Farmers")

	// FarmersLog -
	FarmersLog = logger.DbLog("ecommerce", "Farmers")

	// ProductsCollection -
	ProductsCollection = Connect().Collection("Products")

	// ProductsLog -
	ProductsLog = logger.DbLog("ecommerce", "Products")
)
