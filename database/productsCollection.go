package database

import (
	"context"
	"math"
	"time"

	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// InsertProduct - Inserts the product into the DB
func InsertProduct(ctx context.Context, product model.Product) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	res, err := ProductsCollection.InsertOne(ctx, product)
	if err := HandleProductsError(err, logger.Log()); err != nil {
		return
	}
	ProductsLog.Infof("InsertProduct - NewID: %v", res.InsertedID)
}

// InsertProducts - Allow batch insertion for multiple products
func InsertProducts(ctx context.Context, products []model.Product, farmerID primitive.ObjectID) error {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	// Converts a slice of products into a slice
	// of interface, so that Mongo can parse it
	productsInterface := make([]interface{}, len(products))
	for i := range products {
		products[i].FarmerID = farmerID
		products[i].Price = math.Round(products[i].Price*100) / 100
		productsInterface[i] = products[i]
	}

	res, err := ProductsCollection.InsertMany(ctx, productsInterface)
	if err := HandleProductsError(err, logger.Log()); err != nil {
		return err
	}
	productIds := make([]primitive.ObjectID, len(res.InsertedIDs))
	for i := range res.InsertedIDs {
		productIds[i] = res.InsertedIDs[i].(primitive.ObjectID)
	}
	AddProductsToFarmer(ctx, farmerID, productIds)
	ProductsLog.Infof("InsertProducts - NewIDs: %v", productIds)

	return nil
}
