package database

// HandleCustomerError logs the error to customerLog if there is
// an error. Otherwise, it returns nil
func HandleCustomerError(err error, log string) error {
	if err != nil {
		CustomerLog.Errorf(log+" %v", err.Error())
		return err
	}
	return nil
}

// HandleProductsError logs the error to customerLog if there is
// an error. Otherwise, it returns nil
func HandleProductsError(err error, log string) error {
	if err != nil {
		ProductsLog.Errorf(log+" %v", err.Error())
		return err
	}
	return nil
}

// HandleFarmersError logs the error to customerLog if there is
// an error. Otherwise, it returns nil
func HandleFarmersError(err error, log string) error {
	if err != nil {
		FarmersLog.Errorf(log+" %v", err.Error())
		return err
	}
	return nil
}
