package database

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// Db - stores the established connection as a global variable
// so we do not have to connect each time.
var (
	Db *mongo.Database
)

// var Db *mongo.Database

// Connect - Establishes an initial connection to Mongodb
func Connect() *mongo.Database {
	client, err := mongo.NewClient(options.Client().ApplyURI(os.Getenv("MONGO_URI")))
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
	pingDB(ctx, client)
	defer cancel()

	Db := client.Database("ecommerce")
	return Db
}

func disconnect(client *mongo.Client) {
	ctx, cancel := context.WithTimeout(context.Background(), 50*time.Second)
	client.Disconnect(ctx)
	defer cancel()
}

func pingDB(ctx context.Context, client *mongo.Client) {
	// Ping the primary
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}

	fmt.Println("Successfully connected and pinged.")
}
