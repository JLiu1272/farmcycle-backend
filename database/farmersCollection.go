package database

import (
	"context"
	"time"

	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// InsertFarmers - Add farmers to farmers collection
func InsertFarmers(ctx context.Context, farmers []model.Farmer) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	// Converts a slice of farmers into a slice
	// of interface, so that Mongo can parse it
	farmersInterface := make([]interface{}, len(farmers))
	for i := range farmers {
		farmersInterface[i] = farmers[i]
	}

	_, err := FarmersCollection.InsertMany(ctx, farmersInterface)
	if err := HandleFarmersError(err, logger.Log()); err != nil {
		return
	}
}

// AddProductsToFarmer - Add the product ids to farmer's products array
func AddProductsToFarmer(ctx context.Context, farmerID primitive.ObjectID, productIds []primitive.ObjectID) {
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()
	filter := bson.M{"_id": farmerID}

	updateResult, err := FarmersCollection.UpdateOne(ctx,
		filter,
		bson.D{
			primitive.E{
				Key: "$addToSet", Value: bson.D{primitive.E{
					Key:   "products",
					Value: bson.M{"$each": productIds},
				}},
			},
		})
	if err := HandleFarmersError(err, logger.Log()); err != nil {
		return
	}

	FarmersLog.Infof("%v: Matched %v documents and updated %v documents.\n", logger.FuncName(), updateResult.MatchedCount, updateResult.ModifiedCount)
}

// FindAllFarmers - Get the entire list of farmers
func FindAllFarmers(ctx context.Context) []*model.Farmer {
	// Find method options
	// TODO: Set some options, as we may not want
	// to obtain all the products at once
	findOptions := options.Find()

	// Stores the decoded result in here
	var farmers []*model.Farmer

	// Passing bson.D{{}} as the filter matches all documents in the collection
	cur, err := FarmersCollection.Find(ctx, bson.D{{}}, findOptions)
	if err := HandleFarmersError(err, logger.Log()); err != nil {
		return make([]*model.Farmer, 0)
	}

	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	for cur.Next(ctx) {

		// create a value into which the single document can be decoded
		var farmer model.Farmer
		err := cur.Decode(&farmer)
		if err := HandleFarmersError(err, logger.Log()); err != nil {
			return make([]*model.Farmer, 0)
		}

		farmers = append(farmers, &farmer)
	}

	// Close the cursor once finished
	cur.Close(ctx)
	return farmers
}

// FindFarmerByID - Find the farmer detailed info based on farmer id
func FindFarmerByID(ctx context.Context, farmerID string) ([]model.Farmer, error) {
	opts := options.FindOne()
	var farmerInfo model.Farmer
	var result []model.Farmer

	oid, err := primitive.ObjectIDFromHex(farmerID)
	if err != nil {
		HandleFarmersError(err, logger.Log())
		return result, err
	}

	err = FarmersCollection.FindOne(ctx, bson.D{{"_id", oid}}, opts).Decode(&farmerInfo)
	if err != nil {
		// ErrNoDocuments means that the filter did not match any documents in the collection
		if err == mongo.ErrNoDocuments {
			return result, nil
		}
		HandleFarmersError(err, logger.Log())
		return result, err
	}

	result = append(result, farmerInfo)
	return result, nil
}

// FarmerIDExist returns true if farmer exist, else false
func FarmerIDExist(ctx context.Context, farmerID string) (bool, error) {
	farmerInfos, err := FindFarmerByID(ctx, farmerID)
	if err != nil {
		return false, err
	}

	if len(farmerInfos) == 0 {
		return false, nil
	}
	return true, nil
}
