package productscollection

import (
	"context"

	"farmcycle.us/user/farmcycle/database"
	"farmcycle.us/user/farmcycle/helper"
	"farmcycle.us/user/farmcycle/logger"
	"farmcycle.us/user/farmcycle/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// FindAllProductsByTag - Find all the products in the products collection
func FindAllProductsByTag(ctx context.Context) (map[string]model.Product, error) {
	findOptions := options.Find()
	query := bson.M{}

	products, err := findProducts(ctx, query, findOptions, nil)
	if err != nil {
		return nil, err
	}
	return products, nil
}

// FindAllProductsByIds queries product details of product ids
// in a customer's cart, and returns a map structure containing product details
// for the cart
func FindAllProductsByIds(ctx context.Context, items map[string]int) (map[string]model.Product, error) {
	productIds, err := helper.MapToObjIDArr(items)

	if err := database.HandleProductsError(err, logger.Log()); err != nil {
		return nil, err
	}

	query := bson.M{"_id": bson.M{"$in": productIds}}
	findOptions := options.Find()

	// Obtain the product details in the items dic
	products, err := findProducts(ctx, query, findOptions, items)
	if err := database.HandleProductsError(err, logger.Log()); err != nil {
		return nil, err
	}

	return products, nil
}

// FindProductByID returns the product details given product id
func FindProductByID(ctx context.Context, productIDs []string) ([]model.Product, error) {
	productOids, err := helper.StrToObjectIDArr(productIDs)

	if err != nil {
		return nil, err
	}

	query := bson.M{"_id": bson.M{"$in": productOids}}
	findOptions := options.Find()

	cursor, err := database.ProductsCollection.Find(ctx, query, findOptions)
	if err != nil {
		return nil, err
	}

	var products []model.Product
	if err = cursor.All(ctx, &products); err != nil {
		return nil, err
	}

	return products, nil
}

// FindProductByKeyword searches for products that have matching itemName
// with keyword
func FindProductByKeyword(ctx context.Context, keyword string) ([]model.Product, error) {
	query := bson.M{
		"$text": bson.M{"$search": keyword},
	}

	findOptions := options.Find().SetProjection(bson.M{"score": bson.M{"$meta": "textScore"}}).SetSort(bson.M{"score": bson.M{"$meta": "textScore"}})

	cursor, err := database.ProductsCollection.Find(ctx, query, findOptions)
	if err != nil {
		return nil, err
	}

	var products []model.Product
	if err = cursor.All(ctx, &products); err != nil {
		return nil, err
	}

	return products, nil
}

// findProducts finds all products that matches the criteria given in query.
// It will return a dictionary object where the key is the productId, and the
// value is the product detail.
// If item is not nil, it means that the the product detail should include
// the quantity of this item in the cart
func findProducts(ctx context.Context, query bson.M, opts *options.FindOptions, items map[string]int) (map[string]model.Product, error) {
	products := make(map[string]model.Product)
	cur, err := database.ProductsCollection.Find(ctx, query, opts)

	if err := database.HandleProductsError(err, logger.Log()); err != nil {
		return nil, err
	}

	// Finding multiple documents returns a cursor
	// Iterating through the cursor allows us to decode documents one at a time
	for cur.Next(ctx) {

		// create a value into which the single document can be decoded
		var product model.Product
		err := cur.Decode(&product)
		if err := database.HandleProductsError(err, logger.Log()); err != nil {
			return nil, err
		}

		productID := product.ProductID.Hex()

		if items != nil {
			product.UpdateQuantity(items[productID])
		}
		products[productID] = product
	}

	if err := database.HandleProductsError(cur.Err(), logger.Log()); err != nil {
		return nil, err
	}

	// Close the cursor once finished
	cur.Close(ctx)

	return products, nil
}
