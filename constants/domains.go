package constants

const (
	// FarmCycleMainDomain maps Farm Cycle's main e-commerce URL
	FarmCycleMainDomain string = "http://localhost:3000"
)
