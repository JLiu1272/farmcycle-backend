package logger

import (
	"fmt"
	"net/http"
	"os"
	"runtime"
	"strings"

	"github.com/sirupsen/logrus"
)

// Logger is an abstraction class to log
// actions. There are two types of log
// 1) Route - Responsible for logging when an API is requested
// 2) Database - Responsible for logging when making a query to
// 				 the database

var (
	logs = map[string]*logrus.Logger{
		"route":    logrus.New(),
		"database": logrus.New(),
	}
	format = "text"
)

// InitLog Initialise Logging tools
// Parameter -
//	format : Log output format, JSON or Text
// Json format - &log.JSONFormatter{}
// Text format - &log.TextFormatter{}
func init() {
	for _, logPointer := range logs {
		if format == "text" {
			logPointer.SetFormatter(&logrus.TextFormatter{})
		} else if format == "json" {
			logPointer.SetFormatter(&logrus.JSONFormatter{})
		}
		// Write log to file
		// file, err := os.OpenFile("logs/farmcycle_backend_"+fileName+".log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		// if err == nil {
		// 	logPointer.Out = file
		// } else {
		// 	logPointer.Info("Failed to log to " + fileName + ", using default stderr")
		logPointer.SetOutput(os.Stdout)
	}
}

// RouteLog - avoids have to create a log field each time
// reduces amount of code
func RouteLog(r *http.Request) *logrus.Entry {
	routeLog := logs["route"]
	if r == nil {
		l := routeLog.WithFields(logrus.Fields{
			"method": "Starting Server",
			"msg":    "Starting server",
		})
		return l
	}
	l := routeLog.WithFields(logrus.Fields{
		"method":     r.Method,
		"requestURI": r.RequestURI,
	})
	return l
}

// DbLog - Creates an entry to the database log file
func DbLog(db string, collection string) *logrus.Entry {
	databaseLog := logs["database"]
	l := databaseLog.WithFields(logrus.Fields{
		"db":         db,
		"collection": collection,
	})
	return l
}

// LogInfo - Returns basic logging info,
// fileName:lineNumber functionName
func LogInfo() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return fmt.Sprintf("%s:%d %s", relativePath(frame.File), frame.Line, relativePath(frame.Function))
}

// LogError - turns the log in the ERRORED form
func LogError(logInfo ...string) string {
	// if no logInfo given, we use our own
	// log info
	if len(logInfo) <= 0 {
		pc := make([]uintptr, 15)
		n := runtime.Callers(2, pc)
		frames := runtime.CallersFrames(pc[:n])
		frame, _ := frames.Next()
		return fmt.Sprintf("%s:%d %s", relativePath(frame.File), frame.Line, relativePath(frame.Function))
	}
	return logInfo[0] + " %v"
}

// Log is a more generic function, that is used for logging
// both error and info type function calls
func Log() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return fmt.Sprintf("%s:%d %s", relativePath(frame.File), frame.Line, relativePath(frame.Function))
}

// FuncName - obtains the function name
func FuncName() string {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return relativePath(frame.Function)
}

// relativePath obtains the file name of the file
// path
// (i.e) given /Users/jenniferliu/Documents/Startups/FarmCycle/Backend/farmcycle-backend/database/customerCollection.go
// this function returns customerCollection.go
// Trying to reduce the log file size
func relativePath(fullPath string) string {
	path := strings.Split(fullPath, "/")
	return path[len(path)-1]
}
