package model

// HTTPMsg - a model for how an
// http return will look like
type HTTPMsg struct {
	Action  string
	Message string
}
