package model

// ProductFilter model
type ProductFilter struct {
	Keyword string `validate:"required" json:"keyword"`
}
