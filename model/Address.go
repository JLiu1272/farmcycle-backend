package model

// Address model
type Address struct {
	Type                string `json:"type" bson:"type"`
	AddressLine1        string `validate:"required" json:"addressLine1" bson:"addressLine1"`
	AddressLine2        string `json:"addressLine2" bson:"addressLine2"`
	BusinessName        string `json:"businessName" bson:"businessName"`
	Zipcode             int    `validate:"required" json:"zipcode" bson:"zipcode"`
	DeliveryInstruction string `json:"deliveryInstruction" bson:"deliveryInstruction"`
	IsDefault           bool   `json:"isDefault" bson:"isDefault"`
}

// CustomerAddresses model
type CustomerAddresses struct {
	CustomerID string             `json:"_id" bson:"_id"`
	Addresses  map[string]Address `json:"shippingAddresses" bson:"shippingAddresses"`
}
