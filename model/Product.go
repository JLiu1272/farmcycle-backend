package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Product structure
type Product struct {
	ProductID    primitive.ObjectID `json:"productId,omitempty" bson:"_id,omitempty"`
	ItemName     string             `json:"itemName,omitempty" bson:"itemName,omitempty"`
	FarmerID     primitive.ObjectID `json:"farmerId,omitempty" bson:"farmerId,omitempty"`
	FarmerName   string             `json:"farmerName,omitempty" bson:"farmerName,omitempty"`
	Thumbnail    string             `json:"thumbnail,omitempty" bson:"thumbnail,omitempty"`
	Offer        int                `json:"offer,omitempty" bson:"offer,omitempty"`
	Price        float64            `json:"price,omitempty" bson:"price,omitempty"`
	Description  string             `json:"description,omitempty" bson:"description,omitempty"`
	Quantity     int                `json:"quantity,omitempty" bson:"quantity,omitempty"`
	Availability int                `json:"availability,omitempty" bson:"availability,omitempty"`
}

// UpdateQuantity changes the quantity value of the Product object
func (p *Product) UpdateQuantity(newQuantity int) {
	p.Quantity = newQuantity
}

// Products structure, the structure
// when adding a batch of products
type Products struct {
	FarmerID primitive.ObjectID `json:"farmerId,omitempty" bson:"farmerId,omitempty"`
	Products []Product          `json:"products" bson:"products"`
}
