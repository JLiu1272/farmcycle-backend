package model

// Customer model
type Customer struct {
	CustomerID        string             `json:"customerId,omitempty" bson:"_id,omitempty"`
	Phone             string             `json:"phone" bson:"phone"`
	Name              string             `json:"name" bson:"name"`
	Email             string             `json:"email" bson:"email"`
	ShippingAddresses map[string]Address `validate:"required" json:"shippingAddresses" bson:"shippingAddresses"`
	ShoppingCart      Cart               `json:"shoppingCart" bson:"shoppingCart"`
	Wishlist          []string           `json:"wishlist" bson:"wishlist"`
	FavoriteFarmers   []string           `json:"favoriteFarmers" bson:"favoriteFarmers"`
	PurchaseHistories []string           `json:"purchaseHistories" bson:"purchaseHistories"`
}

// NewCustomer creates a new Customer
// Only fills customerID, phone, name, and email
func NewCustomer(customerID string, phone string, name string, email string) Customer {
	newCustomer := Customer{}
	newCustomer.CustomerID = customerID
	newCustomer.Phone = phone
	newCustomer.Name = name
	newCustomer.Email = email
	newCustomer.ShippingAddresses = map[string]Address{}
	newCustomer.ShoppingCart = Cart{
		Items: map[string]int{},
		Size:  0,
	}

	emptyStrArr := make([]string, 0)
	newCustomer.Wishlist = emptyStrArr
	newCustomer.FavoriteFarmers = emptyStrArr
	newCustomer.PurchaseHistories = emptyStrArr
	return newCustomer
}
