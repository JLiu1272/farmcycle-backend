package model

// IDToken - a model for storing the token to
// verify the authenticity of this user
type IDToken struct {
	IDToken string `json:"id_token"`
}
