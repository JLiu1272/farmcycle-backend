package model

// Cart model
type Cart struct {
	Items map[string]int `validate:"required" json:"items" bson:"items"`
	Size  int            `json:"size" bson:"size"`
}

// CustomerCart model
type CustomerCart struct {
	CustomerID   string `validate:"required" json:"customerId" bson:"_id"`
	ShoppingCart Cart   `validate:"required" json:"cart" bson:"shoppingCart"`
}
