package model

// Receipt model
type Receipt struct {
	Name               string    `validate:"required" json:"name"`
	DeliveryOption     string    `validate:"required" json:"deliveryOption"`
	DeliveryAddress    string    `validate:"required" json:"DeliveryAddress"`
	CustomerID         string    `validate:"required" json:"customerID"`
	PhoneNumber        string    `validate:"required" json:"phoneNumber"`
	DesiredArrivalTime string    `validate:"required" json:"desiredArrivalTime"`
	SellerNote         string    `json:"sellerNote"`
	ShoppingCart       []Product `validate:"required" json:"cart"`
}
