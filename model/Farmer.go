package model

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Farmer - a Farmer structure
type Farmer struct {
	ID            primitive.ObjectID   `json:"id,omitempty" bson:"_id,omitempty"`
	Thumbnail     string               `json:"thumbnail" bson:"thumbnail"`
	FarmName      string               `json:"farmName" bson:"farmName"`
	ContactPerson string               `json:"contactPerson" bson:"contactPerson"`
	City          string               `json:"city" bson:"city"`
	State         string               `json:"state" bson:"state"`
	Slogan        string               `json:"slogan" bson:"slogan"`
	NumSales      int                  `json:"numSales" bson:"numSales"`
	PhoneNumber   int                  `json:"phoneNumber" bson:"phoneNumber"`
	Tags          []string             `json:"tags" bson:"tags"`
	Description   string               `json:"description" bson:"description"`
	Products      []primitive.ObjectID `json:"products" bson:"products"`
	Rating        int                  `json:"rating" bson:"rating"`
	RegistryDate  time.Time            `json:"registryDate" bson:"registryDate"`
}
