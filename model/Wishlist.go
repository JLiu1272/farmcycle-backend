package model

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Wishlist is a struct that holds information about
// the wishlist/favorite products from the given user
type Wishlist struct {
	ProductIDs []primitive.ObjectID `validate:"required" json:"productIds" bson:"wishlist"`
}
